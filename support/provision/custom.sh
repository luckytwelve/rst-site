#!/usr/bin/env bash

echo ">>> Installing project..."

cd /realestate/ > /dev/null 2>&1
cp /realestate/.env.example /realestate/.env > /dev/null 2>&1
npm install > /dev/null 2>&1
npm run build > /dev/null 2>&1
npm run db:migrate > /dev/null 2>&1