#!/usr/bin/env bash

echo ">>> Installing Nginx"

# Add repo for latest stable nginx
sudo add-apt-repository -y ppa:nginx/stable > /dev/null 2>&1

# Update Again
sudo apt-get update > /dev/null 2>&1

# Install Nginx
# -qq implies -y --force-yes
sudo apt-get install -qq nginx > /dev/null 2>&1

# Turn off sendfile to be more compatible with Windows, which can't use NFS
sed -i 's/sendfile on;/sendfile off;/' /etc/nginx/nginx.conf > /dev/null 2>&1
echo "client_max_body_size 50m;" >> /etc/nginx/nginx.conf > /dev/null 2>&1

sudo rm /etc/nginx/sites-available/default > /dev/null
sudo cp /realestate/support/nginx/vhosts/default /etc/nginx/sites-available/default > /dev/null
sudo openssl req -x509 -nodes -days 3650 -subj '/C=CA/ST=QC/L=Montreal/O=TeamMine/CN=realestate.dev' -newkey rsa:1024 -keyout /etc/ssl/private/realestate.dev.key -out /etc/ssl/certs/realestate.dev.crt > /dev/null 2>&1
sudo service nginx restart > /dev/null
