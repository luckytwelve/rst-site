#!/usr/bin/env bash

echo ">>> Installing PostgreSQL"

[[ -z "$1" ]] && { echo "!!! PostgreSQL root password not set. Check the Vagrant file."; exit 1; }

# Set some variables
POSTGRE_VERSION=9.6

# Add PostgreSQL GPG public key
# to get latest stable
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add - > /dev/null 2>&1

# Add PostgreSQL Apt repository
# to get latest stable
sudo touch /etc/apt/sources.list.d/pgdg.list > /dev/null 2>&1
sudo echo "deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main" | sudo tee /etc/apt/sources.list.d/pgdg.list > /dev/null 2>&1

# Update Apt repos
sudo apt-get update > /dev/null 2>&1

# Install PostgreSQL
# -qq implies -y --force-yes
sudo apt-get install -qq postgresql-9.6 postgresql-contrib-9.6 > /dev/null 2>&1

# Configure PostgreSQL
# Listen for localhost connections
sudo sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/g" /etc/postgresql/$POSTGRE_VERSION/main/postgresql.conf > /dev/null 2>&1

# Identify users via "md5", rather than "ident", allowing us
# to make PG users separate from system users. "md5" lets us
# simply use a password
echo "host    all             all             0.0.0.0/0               md5" | sudo tee -a /etc/postgresql/$POSTGRE_VERSION/main/pg_hba.conf > /dev/null 2>&1
sudo service postgresql start > /dev/null 2>&1

# Create new superuser "vagrant"
sudo -u postgres createuser -s vagrant > /dev/null 2>&1

# Create new user "root" w/ defined password
# Not a superuser, just tied to new db "vagrant"
sudo -u postgres psql -c "CREATE ROLE root LOGIN UNENCRYPTED PASSWORD '$1' NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;" > /dev/null 2>&1
sudo -u postgres psql -c "CREATE DATABASE $2;" > /dev/null 2>&1
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE $2 to root;" > /dev/null 2>&1

# Make sure changes are reflected by restarting
sudo service postgresql restart > /dev/null 2>&1
