#!/usr/bin/env bash

echo ">>> Setting Timezone & Locale to $2 & en_US.UTF-8"
sudo ln -sf /usr/share/zoneinfo/$2 /etc/localtime > /dev/null 2>&1
sudo apt-get install -qq language-pack-en > /dev/null 2>&1
sudo locale-gen en_US > /dev/null 2>&1
sudo update-locale LANG=en_US.UTF-8 LC_CTYPE=en_US.UTF-8 > /dev/null 2>&1

echo ">>> Installing Base Packages"

# Update
sudo apt-get update > /dev/null 2>&1

# Install base packages
# -qq implies -y --force-yes
sudo apt-get install -qq curl htop unzip libfontconfig1 libxrender1 git-core ack-grep software-properties-common build-essential cachefilesd > /dev/null 2>&1
sudo apt-get install -y python-software-properties software-properties-common apt-transport-https > /dev/null 2>&1
# Setting up Swap
# Disable case sensitivity
shopt -s nocasematch > /dev/null 2>&1

if [[ ! -z $1 && ! $1 =~ false && $1 =~ ^[0-9]*$ ]]; then
    echo ">>> Setting up Swap ($1 MB)"
    # Create the Swap file
    fallocate -l $1M /swapfile > /dev/null 2>&1
    # Set the correct Swap permissions
    chmod 600 /swapfile > /dev/null 2>&1
    # Setup Swap space
    mkswap /swapfile > /dev/null 2>&1
    # Enable Swap space
    swapon /swapfile > /dev/null 2>&1
    # Make the Swap file permanent
    echo "/swapfile   none    swap    sw    0   0" | tee -a /etc/fstab > /dev/null 2>&1
    # Add some swap settings:
    # vm.swappiness=5: Means that there wont be a Swap file until memory hits 90% useage
    # vm.vfs_cache_pressure=50: read http://rudd-o.com/linux-and-free-software/tales-from-responsivenessland-why-linux-feels-slow-and-how-to-fix-that
    printf "vm.swappiness=5\nvm.vfs_cache_pressure=50" | tee -a /etc/sysctl.conf && sysctl -p > /dev/null 2>&1
fi

# Enable case sensitivity
shopt -u nocasematch > /dev/null 2>&1
# Enable cachefilesd
echo "RUN=yes" > /etc/default/cachefilesd > /dev/null 2>&1