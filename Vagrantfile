# -*- mode: ruby -*-
# vi: set ft=ruby :

server_hostname       = "realestate.dev"
server_ip             = "10.10.10.100"
server_cpus           = "1"   # Cores
server_memory         = "512" # MB
server_swap           = "128" # Options: false | int (MB) - Guideline: Between one or two times the server_memory
server_timezone       = "UTC"

pgsql_forward_port    = 54321  # Port for forwarding (ex. you can use PgAdmin 3/4 with "localhost:54320")
pgsql_root_password   = "root"   # We'll assume user "root"
pgsql_main_database   = "realestate" # Name of main database

nodejs_version        = "8.x"      # By default "latest" will equal the latest stable version
nodejs_packages       = [          # List any global NodeJS packages that you want to install
  "gulp",
  "babel-cli",
  "pm2@latest"
]

Vagrant.configure("2") do |config|
    config.ssh.shell = "bash -c 'BASH_ENV=/etc/profile exec bash'"
    config.vm.box = "ubuntu/xenial64"
    config.vm.hostname = server_hostname
    config.vm.post_up_message = "The VM is now running! Type vagrant ssh to manage this box."
    config.vm.network "private_network", ip: server_ip
    config.vm.network :forwarded_port, guest: 5432, host: pgsql_forward_port
    config.vm.network :forwarded_port, guest: 6379, host: 63791

    config.vm.synced_folder ".", "/realestate",
        :owner => "ubuntu",
        :group => "ubuntu",
        :create => true,
        :mount_options => ["dmode=777","fmode=777"]

    config.vm.provider "virtualbox" do |vb|
        vb.name = server_hostname
        vb.customize ["modifyvm", :id, "--cpus", server_cpus]
        vb.customize ["modifyvm", :id, "--memory", server_memory]
    end

    # Autostart
    config.vm.provision :shell, :inline => 'JAVA_HOME="/usr/lib/jvm/java-8-oracle"', :run => 'always', privileged: false
    config.vm.provision :shell, :inline => 'echo 3 > /proc/sys/vm/drop_caches', :run => 'always'

    # Provision Base Packages
    config.vm.provision "shell", path: "./support/provision/base.sh", args: [server_swap, server_timezone]

    # Provision PostgreSQL
    config.vm.provision "shell", path: "./support/provision/pgsql.sh", args: [pgsql_root_password, pgsql_main_database]

    # Install Nodejs
    config.vm.provision "shell", path: "./support/provision/nodejs.sh", args: nodejs_packages.unshift(nodejs_version)

    # Provision Nginx
    config.vm.provision "shell", path: "./support/provision/nginx.sh"

    # Provision Redis
    config.vm.provision "shell", path: "./support/provision/redis.sh"

    # Provision Custom
    config.vm.provision "shell", path: "./support/provision/custom.sh", privileged: false

    # Autostart
    config.vm.provision :shell, :inline => "cd /realestate && pm2 start ecosystem.config.js > /dev/null 2>&1", :run => 'always', privileged: false
end
