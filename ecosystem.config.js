const path = require('path');
const env = require('dotenv').config();

if(env.error !== undefined) {
  throw new Error("ENOENT: no such file or directory, open '.env'");
  process.exit(1);
}

module.exports = {
  apps : [
    {
      name      : 'Real Estate',
      cwd       : __dirname,
      script    : path.join(__dirname, 'build/boot.js'),
      instances : 1,
      exec_mode : "cluster",
      max_memory_restart: "1G",
      interpreter_args: "--harmony",
      watch: [
        path.join(__dirname, 'build/*'),
        path.join(__dirname, 'build/boot.js')
      ],
      env: env.parsed
    }
  ]
};
