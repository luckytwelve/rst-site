# Real Estate

## Requirements

* Node.js 8.x
* PostgreSQL 9.6
* Redis
* Babel CLI
* Gulp
* PM2

## Development

### Additional Requirements

* [Vagrant](https://www.vagrantup.com)

### Environment

You are a master of your machine and development environment. There is `default` Vagrant box which you can use to kickstart required servers and start developing:

    vagrant up
    vagrant ssh
    cd /realestate

It takes about 20 minutes to get fully provisioned machine when running `vagrant up` for the first time.

If application doesn't started (you can check it by `pm2 list`), you should to run next command:

    cd /realestate/ && pm2 start ecosystem.config.js

### Local Development

Vagrant box contains Nginx configured with named virtual host `https://realestate.dev/` listening on `10.10.10.100:443`. Edit your host's `/etc/hosts` (or it's equivalent) by adding line:

    10.10.10.100 realestate.dev

### Port Forwarding

Virtualbox provides abillity to forward port to VM. Forwarded ports is:

    VM    => Host Machine

    5430  => 54321 PostgreSQL (Database server)
    6379  => 63791 Redis (Cache server)

