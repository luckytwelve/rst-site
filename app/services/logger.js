import winston from 'winston';

winston.emitErrs = true;

const logs_path = __dirname + '/../../storage/logs';
const level = ((process.env.APP_DEBUG == 'true') ? 'debug' : 'info');

var logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      json: false,
      timestamp: true,
      colorize: true,
      level: level,
    }),
    new winston.transports.File({
      filename: logs_path + '/debug.log',
      json: false,
      timestamp: true,
      level: level
    })
  ],
  exceptionHandlers: [
    new (winston.transports.Console)({
      json: false,
      timestamp: true,
      colorize: true
    }),
    new winston.transports.File({
      filename: logs_path + '/exceptions.log',
      json: false
    })
  ],
  exitOnError: false
});

module.exports = logger;
