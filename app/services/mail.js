import config from '../config';
import nodemailer from 'nodemailer';
import smtpTransport from 'nodemailer-smtp-transport';
import mailgunTransport from 'nodemailer-mailgun-transport';
import pepipostTransport from 'nodemailer-pepipost-transport';

class Mail {
  constructor() {
    this.transporters = {
      smtpTransport: smtpTransport,
      mailgunTransport: mailgunTransport,
      pepipostTransport: pepipostTransport
    };

    this.initTransport();
  }

  getTransporter() {
    let transporter = false;
    let settings = config.mail.drivers[config.mail.driver];
    let transporterName = config.mail.driver + 'Transport';

    if(typeof this.transporters[transporterName] === "function") {
      transporter = this.transporters[transporterName](settings);
    }

    return transporter;
  }

  initTransport() {
    let tranp = this.getTransporter();
    let isValid = this.checkConnecting(tranp);

    this.transport = (!isValid) ? false : nodemailer.createTransport(tranp);
  }

  checkConnecting(transporter) {
    return new Promise((resolve, reject) => {
      transporter.verify((error, success) => {
        if(error) {
          console.log('Mail service is unavailable!');
        }

        let valid = (error) ? false : true;
        resolve(valid);
      });
    });
  }

  send(message, callback) {
    if(this.transport) {
      if(!message.from) {
        message.from = config.mail.from;
      }

      this.transport.sendMail(message, (error) => {
        if(error) {
          console.log('Could not to sent email.');
        } else {
          console.log('Mail sent.');
        }
      });
    }
  }
};

module.exports = new Mail;
