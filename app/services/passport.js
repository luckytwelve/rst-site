const database       = require('../database');
const passport       = require('passport');
const LocalStrategy  = require('passport-local').Strategy;

var User = database.models.User;

passport.use(new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password'
}, (email, password, next) => {
  User.findOne({ email : email }, (err, user) => {
    return err 
      ? next(err)
      : user
        ? password === user.password
          ? next(null, user)
          : next(null, false, { message: 'Incorrect password.' })
        : next(null, false, { message: 'Incorrect E-Mail.' });
  });
}));

passport.serializeUser((user, next) => {
  next(null, user.id);
});

passport.deserializeUser((id, next) => {
  User.findById(id, (err,user) => {
    err 
      ? next(err)
      : next(null,user);
  });
});

module.exports = passport;