const mail = {
  driver: process.env.MAIL_DRIVER || 'smtp',
  from: {
    name: process.env.MAIL_FROM_NAME || 'Example',
    address: process.env.MAIL_FROM_ADDRESS || 'hello@example.com'
  },
  drivers: {
    smtp: {
      host: process.env.MAIL_HOST || 'smtp.mailgun.org',
      port: process.env.MAIL_PORT || 587,
      secure: process.env.MAIL_SECURE || false,
      auth: {
          user: process.env.MAIL_USERNAME || '',
          pass: process.env.MAIL_PASSWORD || ''
      }
    },
    mailgun: {
      auth: {
        api_key: process.env.MAILGUN_API_KEY || '',
        domain: process.env.MAILGUN_DOMAIN || 'example.com'
      }
    },
    pepipost: {
        api_key: process.env.PEPIPOST_API_KEY || ''
    }
  }
};

module.exports = mail;
