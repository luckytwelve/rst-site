import { 
  util, 
  express, 
  helmet, 
  cookieParser, 
  bodyParser, 
  database, 
  routes,
  components, 
  services
} from './modules';

global.logger = services.logger;

logger.info(util.format(
  'Starting application using %s environment.',
  process.env.NODE_ENV
));

var app = express();

app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

database.connect().then(db => {
  app.set('db', db);
  // Apply components as middleware
  app = components.applyTo(app);
  // Mount routes
  app.use('/', routes);

  app.listen(process.env.APP_PORT || 3000, () => {
      logger.info(util.format(
        'Worker %d started.',
        process.pid
      ));
  });
});