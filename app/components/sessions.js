const redis   = require("redis");
const session = require('express-session');
const RedisStore = require('connect-redis')(session);

var redisClient = redis.createClient();

module.exports = (app) => {
  app.use(session({
    store: new RedisStore({
      host: 'localhost', 
      port: 6379, 
      client: redisClient,
      ttl: 260,
      prefix: 'session:'
    }),
    saveUninitialized: false,
    resave: false,
    secret: '515de43d9f338a6744ab2a95ec4e3d3b'
  }));

  return app;
};