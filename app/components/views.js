const path = require('path');
const nunjucks = require('nunjucks');

module.exports = (app) => {
  // View engine and path
  app.set('views', path.join(__dirname, '../../resources/views'));
  app.set('view engine', 'njk');

  nunjucks.configure(app.get('views'), {
    autoescape: true,
    express: app
  });

  return app;
};

