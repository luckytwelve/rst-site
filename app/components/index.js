import fs from 'fs';
import path from 'path';

const basename = path.basename(module.filename);

var components = {
  applyTo: function(app) {
    fs.readdirSync(__dirname)
      .filter((file) => {
        return (file.indexOf('.') !== 0) &&
          (file !== basename) &&
          (file.slice(-3) === '.js');
      })
      .forEach((file) => {
        let filename = file.replace('.js', '');
        app = require(path.join(__dirname, file))(app);
      });
    
    return app;
  }
};

module.exports = components;
