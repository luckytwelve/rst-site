const services = require('../services');

module.exports = (app) => {
  app.use(services.passport.initialize());
  app.use(services.passport.session());

  return app;
};