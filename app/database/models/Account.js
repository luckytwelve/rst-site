module.exports = (connection, DataTypes) => {
  var Account = connection.define('Account', {
    type: {
      type: new DataTypes.VIRTUAL(DataTypes.STRING),
      get() {
        return 'accountType';
      }
    },
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    //last_name: DataTypes.STRING,
    //first_name: DataTypes.STRING
  }, {
    underscored: true,
    tableName: 'accounts',
    classMethods: {
      associate: (models) => {
        /*Account.hasMany(models.Article, {
          as: 'authoredArticles' , foreignKey: 'AuthorId'
        });
        User.hasMany(models.Article, {
          as: 'editedArticles' , foreignKey: 'EditorId'
        });
        User.hasMany(models.Comment, {
          as: 'authoredComments' , foreignKey: 'AuthorId'
        });
        User.hasMany(models.Comment, {
          as: 'editedComments' , foreignKey: 'EditorId'
        });
        User.hasMany(models.Token);
        User.hasMany(models.Vote, {
          foreignKey: 'VoterId'
        });
        User.hasMany(models.Flag, {
          foreignKey: 'FlaggerId'
        });*/
      }
    }
  });
  return Account;
};
