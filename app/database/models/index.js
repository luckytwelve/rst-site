import fs from 'fs';
import path from 'path';
import Sequelize from 'sequelize';
import config from '../../config';

const basename = path.basename(module.filename);
const env = process.env.NODE_ENV || 'development';

var db = {};

var connection = new Sequelize(
  config.database[env].database,
  config.database[env].username,
  config.database[env].password,
  config.database[env].options
);

fs
  .readdirSync(__dirname)
  .filter((file) => {
    return (file.indexOf('.') !== 0) &&
      (file !== basename) &&
      (file.slice(-3) === '.js');
  })
  .forEach((file) => {
    var model = connection['import'](path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.connection = connection;
db.Sequelize = Sequelize;

module.exports = db;
