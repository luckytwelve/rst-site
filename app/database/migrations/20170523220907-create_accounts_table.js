'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    queryInterface.createTable('accounts', {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        email: Sequelize.STRING,
        password: Sequelize.STRING,
        last_name: Sequelize.STRING,
        first_name: Sequelize.STRING,
        created_at: {
          type: Sequelize.INTEGER
        },
        updated_at: {
          type: Sequelize.INTEGER
        }
      },
      {
        engine: 'InnoDB',                     // default: 'InnoDB'
        charset: 'utf8',                      // default: null
        schema: 'public'                      // default: public, PostgreSQL only.
      }
    )
  },

  down: function (queryInterface, Sequelize) {
      return queryInterface.dropTable('accounts');
  }
};
