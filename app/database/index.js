import models from './models';

const connect = () => {
  return new Promise((resolve, reject) => {
    try {
      models.connection.sync().then(() => {
        resolve(models);
      });
    } catch (error) {
      reject(error);
    }
  });
};

module.exports = {
  connection: models.connection,
  models: models,
  connect: connect
};
