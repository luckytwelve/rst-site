module.exports = (router, controllers, middleware) => {
  // Main page
  router.get('/', controllers.IndexController.index);

  router.post('/account/login', controllers.AccountController.login);

  // Account Manage
  router.get('/manage', middleware.isAuth, controllers.ManageController.index);

  return router;
};
