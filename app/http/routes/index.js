import fs from 'fs';
import path from 'path';
import express from 'express';

const controllers = require('../controllers');
const middleware = require('../middleware');

const basename = path.basename(module.filename);
var router = express.Router();

fs.readdirSync(__dirname)
  .filter((file) => {
    return (file.indexOf('.') !== 0) &&
      (file !== basename) &&
      (file.slice(-3) === '.js');
  })
  .forEach((file) => {
    let filename = file.replace('.js', '');
    router = require(path.join(__dirname, file))(router, controllers, middleware);
  });

module.exports = router;
