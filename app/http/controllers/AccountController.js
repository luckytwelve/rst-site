const services = require('../../services');

module.exports = {
  login: function(req, res, next) {
    services.passport.authenticate('local',
      function(err, user, info) {
        return err 
          ? next(err)
          : user
            ? req.logIn(user, function(err) {
                return err
                  ? next(err)
                  : res.redirect('/manage');
              })
            : res.redirect('/');
      }
    )(req, res, next);
  }
};